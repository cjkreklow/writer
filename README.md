[![GoDoc reference](https://godoc.org/kreklow.us/go/writer?status.svg)](https://godoc.org/kreklow.us/go/writer)
# Overview
`writer` is a simple output library for Go. The library is currently in
early alpha stages, so no guarantees of stability or correctness are made.

# About
`writer` is maintained by Collin Kreklow. The source code is licensed under
the terms of the MIT license, see `LICENSE.txt` for further information.

