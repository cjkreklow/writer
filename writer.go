// Copyright 2018 Collin Kreklow
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Package writer is a simple output library modelled after the standard
// library Print* functions.
package writer // import "kreklow.us/go/writer"

import (
	"fmt"
	"io"
	"sync"

	"github.com/bbrks/wrap"
)

// Writer is an object that provides methods to print output to the
// io.Writer provided, optionally wrapping the output lines.
type Writer struct {
	// Output is the io.Writer where output will be directed. Commonly
	// this will be os.Stdout or a previously-opened file.
	Output io.Writer

	// WrapLimit is the maximum line length allowed when calling a
	// wrapping method such as WPrintln.
	WrapLimit int

	// Setting Enabled to false will discard any data passed to any
	// method instead of passing it to Output.
	Enabled bool

	m       sync.Mutex
	wrapper wrap.Wrapper
}

// NewWriter returns a Writer with sane defaults.
func NewWriter(output io.Writer, wraplimit int) *Writer {
	w := Writer{
		Output:    output,
		WrapLimit: wraplimit,
		Enabled:   true,
	}

	w.wrapper = wrap.NewWrapper()
	w.wrapper.StripTrailingNewline = true

	return &w
}

// Print formats the provided arguments like fmt.Print.
func (w *Writer) Print(v ...interface{}) {
	if !w.Enabled {
		return
	}
	w.m.Lock()
	defer w.m.Unlock()
	io.WriteString(w.Output, fmt.Sprint(v...))
}

// Printf formats the provided arguments like fmt.Printf.
func (w *Writer) Printf(f string, v ...interface{}) {
	if !w.Enabled {
		return
	}
	w.m.Lock()
	defer w.m.Unlock()
	io.WriteString(w.Output, fmt.Sprintf(f, v...))
}

// Println formats the provided arguments like fmt.Println.
func (w *Writer) Println(v ...interface{}) {
	if !w.Enabled {
		return
	}
	w.m.Lock()
	defer w.m.Unlock()
	io.WriteString(w.Output, fmt.Sprintln(v...))
}

// WPrint formats the provided arguments like fmt.Print, then wraps the
// output at the specified WrapLimit.
func (w *Writer) WPrint(v ...interface{}) {
	if !w.Enabled {
		return
	}
	w.m.Lock()
	defer w.m.Unlock()
	io.WriteString(w.Output, w.wrapper.Wrap(fmt.Sprint(v...), w.WrapLimit))
}

// WPrintf formats the provided arguments like fmt.Printf, then wraps
// the output at the specified WrapLimit.
func (w *Writer) WPrintf(f string, v ...interface{}) {
	if !w.Enabled {
		return
	}
	w.m.Lock()
	defer w.m.Unlock()
	io.WriteString(w.Output, w.wrapper.Wrap(fmt.Sprintf(f, v...), w.WrapLimit))
}

// WPrintln formats the provided arguments like fmt.Println, then wraps
// the output at the specified WrapLimit.
func (w *Writer) WPrintln(v ...interface{}) {
	if !w.Enabled {
		return
	}
	w.m.Lock()
	defer w.m.Unlock()
	io.WriteString(w.Output, w.wrapper.Wrap(fmt.Sprintln(v...), w.WrapLimit))
}
